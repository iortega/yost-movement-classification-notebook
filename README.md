# Yost Movement Classification Notebook

## Euskaraz

Sare Neuronalen arkitekturak:
- Sare neuronal arrunta
- Autoencoder
- GAN (Generative Adversarial Network)
- CNN (Convolutional Neural Network)

Gehiagarria:
- PCA (Principal componen Analysis)

## English

NN architechtures:
- Ordinary Neural Network
- Autoencoder
- GAN (Generative Adversarial Network)
- CNN (Convolutional Neural Network)

Extra:
- PCA (Principal componen Analysis)
